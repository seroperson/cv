# https://makefiletutorial.com/#makefile-cookbook

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

build_docker: Dockerfile
	docker build -t seroperson-cv $(CURDIR)

compile: cv.tex
	docker run -v "$(CURDIR):/mnt/src" -u "$(CURRENT_UID):$(CURRENT_GID)" --rm -it seroperson-cv /usr/bin/latexmk -pdf cv.tex
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=cv-gs-optimized.pdf cv.pdf
	docker run -v "$(CURDIR):/workdir" -u "$(CURRENT_UID):$(CURRENT_GID)" --rm -it ptspts/pdfsizeopt pdfsizeopt cv-gs-optimized.pdf cv-gs-sizeopt-optimized.pdf
	cp $(CURDIR)/cv-gs-sizeopt-optimized.pdf $(CURDIR)/output/cv.pdf
	rm $(CURDIR)/cv-gs-sizeopt-optimized.pdf
	rm $(CURDIR)/cv-gs-optimized.pdf

clean:
	docker run -v "$(CURDIR):/mnt/src" -u "$(CURRENT_UID):$(CURRENT_GID)" --rm -it seroperson-cv /usr/bin/latexmk -c
